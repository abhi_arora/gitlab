import { s__ } from '~/locale';

export const SCAN_EXECUTION_RULES_LABELS = {
  pipeline: s__('ScanExecutionPolicy|A pipeline is run'),
  schedule: s__('ScanExecutionPolicy|Schedule'),
};

export const SCAN_EXECUTION_PIPELINE_RULE = 'pipeline';
export const SCAN_EXECUTION_SCHEDULE_RULE = 'schedule';
